/* FFTW3 Vapi Binding
 *
 * Copyright (c) 2022 Kate Wulff <katty.wulff@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

[CCode (cprefix = "fftw_",
        lower_case_cprefix = "fftw_",
        cheader_filename="fftw3.h")]
namespace Fftw {
    [CCode (cprefix = "FFTW_")]
    public enum Direction {
        FORWARD,
        BACKWARD,
    }

    [CCode (cprefix = "FFTW_")]
    public enum Flags {
        MEASURE,
        DESTROY_INPUT,
        UNALIGNED,
        CONSERVE_MEMORY,
        EXHAUSTIVE,
        PRESERVE_INPUT,
        PATIENT,
        ESTIMATE,
        WISDOM_ONLY
    }

    [Compact]
    [CCode (cname = "fftw_plan",
            free_function = "fftw_destroy_plan",
            has_type_id = false)]
    public class Plan {
        [CCode (cname = "fftw_plan_dft")]
        public Plan (int[] n,
                     [CCode (array_length = false)]
                     double[] in_,
                     [CCode (array_length = false)]
                     complex[] out_,
                     Direction direction,
                     Flags flags);
        [CCode (cname = "fftw_plan_dft_r2c_1d")]
        public Plan.r2c_1d ([CCode (array_length_pos = 0.9)]
                            double[] in_,
                            [CCode (array_length = false)]
                            complex[] out_,
                            Flags flags);
        [CCode (cname = "fftw_plan_dft_c2r_1d")]
        public Plan.c2r_1d ([CCode (array_length_pos = 0.9)]
                            complex[] in_,
                            [CCode (array_length = false)]
                            double[] out_,
                            Flags flags);

        [CCode (cname = "fftw_execute")]
        public void execute ();
    }

    [CCode (cname = "fftw_malloc")]
    void *malloc (size_t n);

    [CCode (cname = "fftw_free")]
    void free (void *p);
}

