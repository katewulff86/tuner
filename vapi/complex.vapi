/* C Complex numbers vapi Binding
 *
 * Copyright (c) 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: ISC
 */

[CCode (cheader_filename = "complex.h",
        cname = "double _Complex",
        default_value = "0.0",
        has_type_id = false)]
[SimpleType]
[FloatingType (rank = 2)]
public struct complex {
    [CCode (cname = "CMPLX")]
    public static complex rect (double real, double imag);
    public static complex polar (double r, double theta) {
        return rect (r * GLib.Math.cos (theta), r * GLib.Math.sin (theta));
    }
    public double real {
        [CCode (cname = "creal")]
        get;
        private set;
    }
    public double imag {
        [CCode (cname = "cimag")]
        get;
        private set;
    }
    [CCode (cname = "cabs")]
    public double abs ();
    public double square_abs () {
        return real * real + imag * imag;
    }
    [CCode (cname = "carg")]
    public double arg ();
    [CCode (cname = "conj")]
    public complex conj ();
    [CCode (cname = "cproj")]
    public complex proj ();
}

[CCode (cheader_filename = "complex.h")]
namespace Cmath {
    public const complex I;

    [CCode (cname = "CMPLX")]
    public complex rect (double real, double imag);

    public complex polar (double r, double theta) {
        return rect (r * GLib.Math.cos (theta), r * GLib.Math.sin (theta));
    }

    [CCode (cname = "creal")]
    public double real (complex z);
    [CCode (cname = "cimag")]
    public double imag (complex z);
    [CCode (cname = "cabs")]
    public double fabs (complex z);
    public double square_abs (complex z) {
        return z.real * z.real + z.imag * z.imag;
    }
    [CCode (cname = "conj")]
    public complex conj (complex z);
    [CCode (cname = "cproj")]
    public complex proj (complex z);

    [CCode (cname = "cexp")]
    public complex exp (complex z);
    [CCode (cname = "clog")]
    public complex log (complex z);
    [CCode (cname = "cpow")]
    public complex pow (complex z);
    [CCode (cname = "csqrt")]
    public complex sqrt (complex z);
    [CCode (cname = "csin")]
    public complex sin (complex z);
    [CCode (cname = "ccos")]
    public complex cos (complex z);
    [CCode (cname = "ctan")]
    public complex tan (complex z);
    [CCode (cname = "casin")]
    public complex asin (complex z);
    [CCode (cname = "cacos")]
    public complex acos (complex z);
    [CCode (cname = "catan")]
    public complex atan (complex z);
    [CCode (cname = "csinh")]
    public complex sinh (complex z);
    [CCode (cname = "ccosh")]
    public complex cosh (complex z);
    [CCode (cname = "ctanh")]
    public complex tanh (complex z);
    [CCode (cname = "casinh")]
    public complex asinh (complex z);
    [CCode (cname = "cacosh")]
    public complex acosh (complex z);
    [CCode (cname = "catanh")]
    public complex atanh (complex z);
}

