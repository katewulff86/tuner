/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class PitchDetectorTestSuite : Test.Suite {
    private Tuner.Pda.PitchDetector pitch_detector;

    public PitchDetectorTestSuite (Tuner.Pda.PitchDetector pitch_detector) {
        this.pitch_detector = pitch_detector;
    }

    public override Test.Test[] tests () {
        AudioSourceFunc[] sources = {noisy_square, noisy_saw};
        Test.Test[] tests = {};
        for (double frequency = 640.0; frequency >= 40.0; frequency /= 2.0) {
            foreach (var source in sources)
                tests += new PitchDetectorTest (pitch_detector, source, frequency);
        }

        return tests;
    }
}

private uint32 random_state;

private uint32 xorshift32 () {
	uint32 x = random_state;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;

	return random_state = x;
}

private double uniform () {
    return xorshift32 () / (1.0 * uint32.MAX);
}

private double fractional_part (double x) {
    return x - Math.floor (x);
}

[CCode (has_target = false)]
private delegate double[] AudioSourceFunc (double freq, int length);

private double[] noisy_square (double freq, int length) {
    double[] output = new double[length];
    random_state = 1;

    for (int n = 0; n < length; ++n) {
        output[n] = uniform () * 0.5;
        if (fractional_part (n * freq) >= 0.5)
            output[n] += 1.0;
    }

    return output;
}

private double[] noisy_saw (double freq, int length) {
    double[] output = new double[length];
    random_state = 1;

    for (int n = 0; n < length; ++n) {
        output[n] = uniform () * 0.5 + fractional_part (n * freq);
    }

    return output;
}

// private double[] noisy_sine (double freq, int length) {
//     double[] output = new double[length];
//     random_state = 1;

//     for (int n = 0; n < length; ++n) {
//         output[n] = uniform () * 0.5 + 0.5 * Math.sin (Math.PI * 2 * n * freq);
//     }

//     return output;
// }

internal class PitchDetectorTest : Test.Case {
    private Tuner.Pda.PitchDetector pitch_detector;
    private AudioSourceFunc source;
    private double freq;

    public PitchDetectorTest (Tuner.Pda.PitchDetector pitch_detector, AudioSourceFunc source, double freq) {
        this.pitch_detector = pitch_detector;
        this.source = source;
        this.freq = freq;
    }

    public override void test () {
        stdout.printf (@"frequency: $freq\n");
        pitch_detector.sample_rate = 44100;
        var length = pitch_detector.required_length;
        var audio = source (freq / 44100.0, length);
        var detected_frequency = pitch_detector.detect (audio);
        Test.assert_flt_ne (detected_frequency, -1.0);
        var diff = Tuner.cents (detected_frequency / freq);
        stdout.printf (@"detected: $detected_frequency\n");
        Test.assert_flt_le (Math.fabs (diff), 2.0);
    }
}

