/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Utility : Test.Suite {
    public override Test.Test[] tests () {
        return {
            new Average (),
            new StdDev (),
            new NormaliseStats (),
        };
    }
}

internal class Average : Test.Case {
    public override void test () {
        var avg = Tuner.average ({0.0, 3.0, 6.0});
        Test.assert_flt_eq (avg, 3,0);
    }
}

internal class StdDev : Test.Case {
    public override void test () {
        var std = Tuner.std ({3.0, 5.0, 7.0});
        Test.assert_flt_eq (std, 1.632993162);
    }
}

internal class NormaliseStats : Test.Case {
    public override void test () {
        double[] samples = {2.0, 3.0, 5.0, 7.0, 11.0};
        Tuner.normalise_stats (samples);

        var avg = Tuner.average (samples);
        Test.assert_flt_eq (avg, 0.0);

        var std = Tuner.std (samples);
        Test.assert_flt_eq (std, 1.0);
    }
}

