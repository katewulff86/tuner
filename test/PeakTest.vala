/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Peak : Test.Suite {
    public override Test.Test[] tests () {
        return {
            new SimpleQuadratic (),
        };
    }
}

internal class SimpleQuadratic : Test.Case {
    public override void test () {
        var peak = Tuner.Pda.quadratic_peak ({0.0, 1.0, 0.0});
        Test.assert_flt_eq (peak.frequency, 0.0);
        Test.assert_flt_eq (peak.amplitude, 1.0);
    }
}

