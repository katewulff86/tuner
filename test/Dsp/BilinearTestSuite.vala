/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Dsp {
    public class BilinearTestSuite : Test.Suite {
        public override Test.Test[] tests () {
            return {
                new FrequencyScaleFirstOrderLowpassTest (),
                new FrequencyScaleSecondOrderLowpassTest (),
                new FrequencyScaleFirstOrderHighpassTest (),
                new FrequencyScaleSecondOrderHighpassTest (),
                new LowpassToHighpassFirstOrderTest (),
                new LowpassToHighpassSecondOrderTest (),
                new BilinearTransformFirstOrderLowpassTest (),
                new BilinearTransformFirstOrderHighpassTest (),
                new BilinearTransformSecondOrderLowpassTest (),
                new BilinearTransformSecondOrderHighpassTest (),
            };
        }
    }

    class FrequencyScaleFirstOrderLowpassTest : Test.Case {
        public override void test () {
            var filter = Tuner.Dsp.rational_function (1.0, {}, {}, {-1.0}, {});
            filter = Tuner.Dsp.rational_function_scale (filter, 2.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.0), 1.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 2.0), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 4.0), 1.0/5.0);
        }
    }

    class FrequencyScaleSecondOrderLowpassTest : Test.Case {
        public override void test () {
            var theta = 3.0 * Math.PI / 4.0;
            var filter = Tuner.Dsp.rational_function (1.0, {}, {}, {}, {Tuner.polar (1.0, theta)});
            filter = Tuner.Dsp.rational_function_scale (filter, 2.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.0), 1.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 2.0), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 4.0), 1.0/17.0);
        }
    }

    class FrequencyScaleFirstOrderHighpassTest : Test.Case {
        public override void test () {
            var filter = Tuner.Dsp.rational_function (1.0, {0.0}, {}, {-1.0}, {});
            filter = Tuner.Dsp.rational_function_scale (filter, 2.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.0), 0.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 2.0), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 1.0), 1.0/5.0);
        }
    }

    class FrequencyScaleSecondOrderHighpassTest : Test.Case {
        public override void test () {
            var theta = 3.0 * Math.PI / 4.0;
            var filter = Tuner.Dsp.rational_function (1.0, {0.0, 0.0}, {}, {}, {Tuner.polar (1.0, theta)});
            filter = Tuner.Dsp.rational_function_scale (filter, 2.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.0), 0.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 2.0), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 1.0), 1.0/17.0);
        }
    }

    class LowpassToHighpassFirstOrderTest : Test.Case {
        public override void test () {
            var filter = Tuner.Dsp.rational_function (1.0, {}, {}, {-1.0}, {});
            filter = Tuner.Dsp.rational_function_invert (filter);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.0), 0.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 1.0), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.5), 1.0/5.0);
        }
    }

    class LowpassToHighpassSecondOrderTest : Test.Case {
        public override void test () {
            var theta = 3.0 * Math.PI / 4.0;
            var filter = Tuner.Dsp.rational_function (1.0, {}, {}, {}, {Tuner.polar (1.0, theta)});
            filter = Tuner.Dsp.rational_function_invert (filter);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.0), 0.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 1.0), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_s_gain (filter, 0.5), 1.0/17.0);
        }
    }

    class BilinearTransformFirstOrderLowpassTest : Test.Case {
        public override void test () {
            var omega = 10.0;
            var sample_rate = 100.0;
            var filter = Tuner.Dsp.rational_function (1.0, {}, {}, {-1.0}, {});
            filter = Tuner.Dsp.rational_function_scale (filter, omega);
            filter = Tuner.Dsp.rational_function_bilinear_transform (filter, sample_rate, omega);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, 0.0), 1.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, omega / sample_rate), 0.5);
            Test.assert_flt_lt (Tuner.Dsp.rational_function_z_gain (filter, Math.PI), 1e-30);
        }
    }

    class BilinearTransformFirstOrderHighpassTest : Test.Case {
        public override void test () {
            var omega = 10.0;
            var sample_rate = 100.0;
            var filter = Tuner.Dsp.rational_function (1.0, {0.0}, {}, {-1.0}, {});
            filter = Tuner.Dsp.rational_function_scale (filter, omega);
            filter = Tuner.Dsp.rational_function_bilinear_transform (filter, sample_rate, omega);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, 0.0), 0.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, omega / sample_rate), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, Math.PI), 1.0);
        }
    }

    class BilinearTransformSecondOrderLowpassTest : Test.Case {
        public override void test () {
            var omega = 10.0;
            var sample_rate = 100.0;
            var theta = 3.0 * Math.PI / 4.0;
            var filter = Tuner.Dsp.rational_function (1.0, {}, {}, {}, {Tuner.polar (1.0, theta)});
            filter = Tuner.Dsp.rational_function_scale (filter, omega);
            filter = Tuner.Dsp.rational_function_bilinear_transform (filter, sample_rate, omega);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, 0.0), 1.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, omega / sample_rate), 0.5);
            Test.assert_flt_lt (Tuner.Dsp.rational_function_z_gain (filter, Math.PI), 1e-30);
        }
    }

    class BilinearTransformSecondOrderHighpassTest : Test.Case {
        public override void test () {
            var omega = 10.0;
            var sample_rate = 100.0;
            var theta = 3.0 * Math.PI / 4.0;
            var filter = Tuner.Dsp.rational_function (1.0, {0.0, 0.0}, {}, {}, {Tuner.polar (1.0, theta)});
            filter = Tuner.Dsp.rational_function_scale (filter, omega);
            filter = Tuner.Dsp.rational_function_bilinear_transform (filter, sample_rate, omega);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, 0.0), 0.0);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, omega / sample_rate), 0.5);
            Test.assert_flt_eq (Tuner.Dsp.rational_function_z_gain (filter, Math.PI), 1.0);
        }
    }
}

