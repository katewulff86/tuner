/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Dsp {
    public class FirstOrderTestSuite : Test.Suite {
        public override Test.Test[] tests () {
            return {
                new FirstOrderLowpassTest (),
                new FirstOrderHighpassTest (),
            };
        }
    }

    class FirstOrderLowpassTest : Test.Case {
        public override void test () {
            var a = 0.5;
            var filter = new Tuner.Dsp.FirstOrderFilter (1.0, 1.0, -a);
            double[] impulse = {1.0, 0.0, 0.0, 0.0};
            filter.process_in_place (impulse);

            Test.assert_flt_eq (impulse[0], 1.0);
            Test.assert_flt_eq (impulse[1], 1.0 + a);
            Test.assert_flt_eq (impulse[2], a + a*a);
            Test.assert_flt_eq (impulse[3], a*a + a*a*a);
        }
    }

    class FirstOrderHighpassTest : Test.Case {
        public override void test () {
            var a = 0.5;
            var filter = new Tuner.Dsp.FirstOrderFilter (1.0, -1.0, -a);
            double[] impulse = {1.0, 0.0, 0.0, 0.0};
            filter.process_in_place (impulse);

            Test.assert_flt_eq (impulse[0], 1.0);
            Test.assert_flt_eq (impulse[1], a - 1.0);
            Test.assert_flt_eq (impulse[2], a*a - a);
            Test.assert_flt_eq (impulse[3], a*a*a - a*a);
        }
    }
}

