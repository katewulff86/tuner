/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Dsp {
    public class BiquadTestSuite : Test.Suite {
        public override Test.Test[] tests () {
            return {
                new BiquadLowpassTest (),
                new BiquadHighpassTest (),
            };
        }
    }

    class BiquadLowpassTest : Test.Case {
        public override void test () {
            var a = 0.5;
            var filter = new Tuner.Dsp.BiquadFilter (1.0, 2.0, 1.0, -2*a, a*a);
            double[] impulse = {1.0, 0.0, 0.0, 0.0};
            filter.process_in_place (impulse);

            Test.assert_flt_eq (impulse[0], 1.0);
            Test.assert_flt_eq (impulse[1], 2.0 + 2*a);
            Test.assert_flt_eq (impulse[2], 1.0 + 4*a + 3*a*a);
            Test.assert_flt_eq (impulse[3], 2*a + 6*a*a + 4*a*a*a);
        }
    }

    class BiquadHighpassTest : Test.Case {
        public override void test () {
            var a = 0.5;
            var filter = new Tuner.Dsp.BiquadFilter (1.0, -2.0, 1.0, -2*a, a*a);
            double[] impulse = {1.0, 0.0, 0.0, 0.0};
            filter.process_in_place (impulse);

            Test.assert_flt_eq (impulse[0], 1.0);
            Test.assert_flt_eq (impulse[1], -2 + 2*a);
            Test.assert_flt_eq (impulse[2], 1 - 4*a + 3*a*a);
            Test.assert_flt_eq (impulse[3], 2*a - 6*a*a + 4*a*a*a);
        }
    }
}

