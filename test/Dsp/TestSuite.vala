/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Dsp {
    public class TestSuite : Test.Suite {
        public override Test.Test[] tests () {
            return {
                new FirstOrderTestSuite (),
                new BiquadTestSuite (),
                new BilinearTestSuite (),
            };
        }
    }
}

