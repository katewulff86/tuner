/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class TunerSuite : Test.Suite {
    public override Test.Test[] tests () {
        var pitch_detector_factory = new Tuner.Pda.SimplePitchDetectorFactory ();
        var pitch_detector = new Tuner.Pda.CompoundPitchDetector (pitch_detector_factory);

        return {
            new FourierTransform (),
            new Peak (),
            new Utility (),
            new PitchDetectorTestSuite (pitch_detector),
            new Dsp.TestSuite (),
        };
    }
}

int main (string[] args) {
    var test_suite = new TunerSuite ();
    var runner = new Test.Runner (test_suite);

    return runner.run (args);
}


