/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class FourierTransform : Test.Suite {
    public override Test.Test[] tests () {
        return {
            new ImpulseTest (),
        };
    }
}

internal class ImpulseTest : Test.Case {
    public override void test () {
        var dft = new Tuner.Dsp.Dft.FftwFourierTransform (4);
        var res = dft.transform ({1.0, 0.0, 0.0, 0.0});
        foreach (var amp in res)
            Test.assert_flt_eq (Tuner.magnitude (amp), 1.0);
    }
}

