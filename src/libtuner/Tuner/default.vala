/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public Dsp.Dft.FourierTransform make_fourier_transform (int input_length,
                                                           int oversample = 1,
                                                           Dsp.Dft.WindowFunction window_function = Dsp.Dft.rectangular_window)
            requires (input_length >= 1)
            requires (oversample >= 1) {
        var base_transform = new Dsp.Dft.FftwFourierTransform (input_length * oversample);
        Dsp.Dft.FourierTransform oversampled_transform = base_transform;
        if (oversample > 1)
            oversampled_transform = new Dsp.Dft.OversampledFourierTransformDecorator (base_transform, input_length);
        Dsp.Dft.FourierTransform windowed_transform = oversampled_transform;
        if (window_function != Dsp.Dft.rectangular_window)
            windowed_transform = new Dsp.Dft.WindowedFourierTransformDecorator (oversampled_transform, window_function);

        return windowed_transform;
    }

    public AudioPlumber make_audio_plumber () {
        return new GStreamer.AudioPlumber ();
    }

    public Pda.PitchDetector make_pitch_detector () {
        var factory = new Pda.SimplePitchDetectorFactory ();

        return new Pda.CompoundPitchDetector (factory);
    }

    public void make_timeout_and_plumber (out AudioPlumber plumber, out AnalyserTimeout timeout) {
        plumber = make_audio_plumber ();
        var pitch_detector = make_pitch_detector ();
        var analyser = new Analyser (pitch_detector, plumber.audio_source);
        timeout = new AnalyserTimeout (analyser, 1.0 / 30.0);
    }
}

