/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public interface AsynchronousAudioSource : Object {
        public abstract int sample_rate { get; protected set; }
        public abstract signal void sample_rate_changed (int sample_rate);
        public abstract signal void audio_available (double[] samples);
    }
}

