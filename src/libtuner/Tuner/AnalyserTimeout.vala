/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public class AnalyserTimeout : Object {
        private Analyser analyser;
        private bool cancel_timeout = false;

        public signal void pitch_detected (double pitch);

        public AnalyserTimeout (Analyser analyser, double interval) {
            this.analyser = analyser;
            Timeout.add_full (Priority.DEFAULT, (uint) (interval * 1000.0), timeout);
        }

        ~AnalyserTimeout () {
            cancel_timeout = true;
        }

        private bool timeout () {
            pitch_detected (analyser.analyse ());

            return !cancel_timeout;
        }
    }
}

