/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public class Buffer : Object {
        private double[] buffer = {};
        private int buffer_pos = 0;
        private Mutex mutex;

        public Buffer (int length) {
            buffer = new double[length];
            mutex = Mutex ();
        }

        public void push (double[] samples)
                requires (buffer.length >= samples.length) {
            mutex.lock ();
            copy (buffer, buffer_pos, samples, 0, samples.length);
            buffer_pos += samples.length;
            while (buffer_pos > buffer.length)
                buffer_pos -= buffer.length;
            mutex.unlock ();
        }

        public void pull (double[] samples)
                requires (buffer.length >= samples.length) {
            mutex.lock ();
            var buffer_pos = this.buffer_pos - samples.length;
            while (buffer_pos < 0)
                buffer_pos += buffer.length;
            copy (samples, 0, buffer, buffer_pos, samples.length);
            mutex.unlock ();
        }

        private void copy (double[] to, int to_pos, double[] from, int from_pos, int length) {
            while (length > 0) {
                var samples_left_in_to = to.length - to_pos;
                var samples_left_in_from = from.length - from_pos;
                var samples_to_copy = int.min (length,
                                               int.min (samples_left_in_to, samples_left_in_from));
                Posix.memcpy (&to[to_pos], &from[from_pos], samples_to_copy * sizeof (double));
                length -= samples_to_copy;
                to_pos += samples_to_copy;
                if (to_pos == to.length)
                    to_pos = 0;
                from_pos += samples_to_copy;
                if (from_pos == from.length)
                    from_pos = 0;
            }
        }
    }
}

