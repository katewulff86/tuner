/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public interface AudioPlumber : Object {
        public abstract AudioSource audio_source { get; protected set; }
        public abstract bool mute { get; set; }
    }
}

