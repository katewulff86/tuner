/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Pda {
    public interface PitchDetectorFactory : Object {
        public abstract PitchDetector make_pitch_detector (int length);
    }
}

