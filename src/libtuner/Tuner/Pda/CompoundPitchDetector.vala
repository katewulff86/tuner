/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Pda {
    public class CompoundPitchDetector : Object, PitchDetector {
        private const double BASE_FREQUENCY = 400.0;
        private const double LOWEST_FREQUENCY = 40.0;
        private const int MIN_CYCLES = 10;
        private PitchDetectorFactory pitch_detector_factory;
        private Gee.HashMap<int, PitchDetector> pitch_detectors;
        private PitchDetector base_pitch_detector;
        private int base_sample_length;
        private int max_sample_length;
        private int sample_rate_;

        public int required_length { get; protected set; }
        public int sample_rate {
            get {
                return sample_rate_;
            }
            set {
                initialise (value);
            }
        }

        public CompoundPitchDetector (PitchDetectorFactory pitch_detector_factory) {
            this.pitch_detector_factory = pitch_detector_factory;
        }

        public double detect (double[] buffer) {
            var frequency_estimate = base_pitch_detector.detect (buffer);
            if (frequency_estimate == -1.0)
                return -1.0;
            var estimate_sample_length = required_sample_length (frequency_estimate);
            if (estimate_sample_length <= base_sample_length)
                return frequency_estimate;
            if (estimate_sample_length >= max_sample_length)
                estimate_sample_length = max_sample_length;
            var pitch_detector = pitch_detectors[estimate_sample_length];

            return pitch_detector.detect (buffer);
        }

        private void initialise (int sample_rate) {
            sample_rate_ = sample_rate;
            initialise_pitch_detectors ();
            required_length = max_sample_length;
        }

        private void initialise_pitch_detectors () {
            base_sample_length = required_sample_length (BASE_FREQUENCY);
            max_sample_length = required_sample_length (LOWEST_FREQUENCY);
            pitch_detectors = new Gee.HashMap<int, PitchDetector> ();
            var sample_length = base_sample_length;
            while (sample_length <= max_sample_length) {
                pitch_detectors[sample_length] = pitch_detector_factory.make_pitch_detector (sample_length);
                pitch_detectors[sample_length].sample_rate = sample_rate;
                sample_length *= 2;
            }
            base_pitch_detector = pitch_detectors[base_sample_length];
        }

        private int next_power_of_two (int x) {
            int a = 1;
            while (a < x)
                a *= 2;
            return a;
        }

        private int required_sample_length (double frequency) {
            var sample_length = MIN_CYCLES * sample_rate / frequency;

            return next_power_of_two ((int) sample_length);
        }
    }
}

