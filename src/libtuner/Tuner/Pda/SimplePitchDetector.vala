/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Pda {
    public class SimplePitchDetector : Object, PitchDetector {
        private const int OVERSAMPLE = 16;
        private Dsp.Dft.WindowFunction WINDOW_FUNCTION = Dsp.Dft.sine_window;
        private double peak_threshold;
        private Dsp.Dft.FourierTransform fourier_transform;
        private double[] amplitudes;
        private double LOWEST_FREQUENCY = 40.0;
        private double HIGHEST_FREQUENCY = 720.0;
        private double support_required;

        public int sample_rate { get; set; }
        public int required_length { get; protected set; }

        public SimplePitchDetector (int length) {
            required_length = length;
            fourier_transform = make_fourier_transform (length, OVERSAMPLE, WINDOW_FUNCTION);
            amplitudes = new double[fourier_transform.result_length];
            peak_threshold = length * length / 8192.0;
            support_required = length * length / 512.0;
        }

        public double detect (double[] buffer) {
            double[] end_of_buffer = buffer[buffer.length-required_length:buffer.length];
            var freqs = fourier_transform.transform (end_of_buffer);
            spectrum_log_magnitude (freqs, amplitudes);
            normalise_average (amplitudes);
            var peaks = spectrum_peaks (amplitudes, sample_rate, peak_threshold);

            var fundamentals = find_fundamentals (peaks);
            if (fundamentals.is_empty)
                return -1.0;

            var fundamental = best_support (fundamentals);
            if (fundamental.amplitude < support_required)
                return -1.0;

            return fundamental.frequency;
        }

        private Gee.List<Peak?> find_fundamentals (Gee.List<Peak?> peaks) {
            var fundamentals = new Gee.ArrayList<Peak?> ();
            foreach (var peak in peaks) {
                if (HIGHEST_FREQUENCY >= peak.frequency >= LOWEST_FREQUENCY)
                    fundamentals.add (fundamental_support (peak.frequency, peaks));
            }

            return fundamentals;
        }

        private Peak best_support (Gee.List<Peak?> fundamentals) {
            var best_fundamental = fundamentals[0];

            foreach (var fundamental in fundamentals)
                if (fundamental.amplitude > best_fundamental.amplitude)
                    best_fundamental = fundamental;

            return best_fundamental;
        }
    }
}

