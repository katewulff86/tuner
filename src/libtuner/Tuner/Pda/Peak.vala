/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Pda {
    public struct Peak {
        public double frequency;
        public double amplitude;
    }

    public Peak quadratic_peak (double[] samples)
            requires (samples.length == 3) {
        var a = samples[1];
        var b = 0.5 * (samples[2] - samples[0]);
        var c = 0.5 * (samples[2] - 2*a + samples[0]);
        var x = -0.5 * b / c;
        var y = a + x * (b + c * x);

        return {x, y};
    }

    public Peak fundamental_support (double fundamental, Gee.List<Peak?> peaks) {
        var frequency = 0.0;
        var support = 0.0;
        var weight = 0.0;

        foreach (var peak in peaks) {
            var number = harmonic_number (fundamental, peak.frequency);
            if (number == 0)
                continue;
            if (number > 0) {
                support += peak.amplitude;
                frequency += peak.frequency / number;
                weight += 1;
            } else {
                support -= peak.amplitude;
            }
        }

        return {frequency / weight, support};
    }

    public bool local_peak (double[] samples)
            requires (samples.length == 3) {
        return samples[1] > samples[0] &&
               samples[1] > samples[2];
    }

    public Gee.List<Peak?> spectrum_peaks (double[] spectrum, double sample_rate, double threshold) {
        var peaks = new Gee.ArrayList<Peak?> ();
        var frequency_factor = sample_rate * 0.5 / (spectrum.length - 1);

        for (int n = 1; n < spectrum.length - 1; ++n) {
            unowned double[] samples = spectrum[n-1:n+2];
            if (local_peak (samples)) {
                var peak = quadratic_peak (samples);
                if (peak.amplitude < threshold)
                    continue;
                if (peak.frequency < -1.0 ||
                    peak.frequency > 1.0)
                    continue;
                peak.frequency += n;
                peak.frequency *= frequency_factor;
                peaks.add (peak);
            }
        }

        return peaks;
    }

    public Gee.List<Peak?> filter_peaks (Gee.List<Peak?> peaks, double threshold) {
        var filtered_peaks = new Gee.ArrayList<Peak?> ();
        foreach (var peak in peaks)
            if (peak.amplitude >= threshold)
                filtered_peaks.add (peak);

        return filtered_peaks;
    }
}

