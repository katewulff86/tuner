/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Pda {
    public interface PitchDetector : Object {
        public abstract int sample_rate { get; set; }
        public abstract int required_length { get; protected set; }
        public abstract double detect (double[] buffer);
    }
}

