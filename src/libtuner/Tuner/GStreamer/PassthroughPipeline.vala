/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.GStreamer {
    public class PassthroughPipeline : Object {
        private bool mute_;
        public bool mute {
            get { return mute_; }
            set {
                mute_ = value;
                if (value) {
                    if (pipeline.set_state (Gst.State.PAUSED) == Gst.StateChangeReturn.FAILURE) {
                        critical ("unable to stop");
                    }
                } else {
                    if (pipeline.set_state (Gst.State.PLAYING) == Gst.StateChangeReturn.FAILURE) {
                        critical ("unable to play");
                    }
                }
            }
        }
        private Gst.Pipeline pipeline;

        public PassthroughPipeline() {
            var src = Gst.ElementFactory.make ("autoaudiosrc", "src");
            var sink = Gst.ElementFactory.make ("autoaudiosink", "sink");

            if (src == null | sink == null)
                critical ("could not create all elements");

            pipeline = new Gst.Pipeline("pipeline");
            pipeline.add_many(src, sink);

            if (!src.link (sink)) {
                critical ("could not link sink");
            }
            mute = true;
        }
    }
}

