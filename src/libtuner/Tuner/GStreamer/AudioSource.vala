/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.GStreamer {
    public class AudioSource : Gst.Audio.Filter, Tuner.AsynchronousAudioSource {
        public int sample_rate { get; protected set; }
        private int channels;

        static construct {
            Gst.Audio.Format[] formats = { Gst.Audio.Format.F64 };
            var caps = Gst.Audio.audio_make_raw_caps (formats, Gst.Audio.Layout.INTERLEAVED);
            add_pad_templates (caps);
        }

        public override bool setup (Gst.Audio.Info info) {
            if (info.rate != sample_rate)
                sample_rate_changed (info.rate);
            sample_rate = info.rate;
            channels = info.channels;

            return true;
        }

        public override Gst.FlowReturn transform_ip (Gst.Buffer in_buffer) {
            Gst.MapInfo map_info;
            in_buffer.map (out map_info, Gst.MapFlags.READ);
            unowned double[] data = (double[]) map_info.data;
            if (channels == 1) {
                audio_available (data);
            } else {
                var buffer = downmix (data);
                audio_available (buffer);
            }
            in_buffer.unmap (map_info);

            return Gst.FlowReturn.OK;
        }

        private double[] downmix (double[] in_buffer) {
            double[] out_buffer = new double[in_buffer.length / channels];
            double factor = 1.0 / channels;
            int index = 0;
            for (var i = 0; i < out_buffer.length; ++i) {
                double sum = 0.0;
                for (var j = 0; j < channels; ++j) {
                    sum += in_buffer[index++];
                }

                out_buffer[i] = sum * factor;
            }

            return out_buffer;
        }
    }
}

