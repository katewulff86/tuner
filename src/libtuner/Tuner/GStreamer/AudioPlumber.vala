/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.GStreamer {
    public class AudioPlumber : Object, Tuner.AudioPlumber {
        public Tuner.AudioSource audio_source { get; protected set; }
        public bool mute {
            get {
                return passthrough_pipeline.mute;
            }
            set {
                passthrough_pipeline.mute = value;
            }
        }
        private InputPipeline input_pipeline;
        private PassthroughPipeline passthrough_pipeline;

        public AudioPlumber () {
            input_pipeline = new InputPipeline ();
            passthrough_pipeline = new PassthroughPipeline ();
            audio_source = new AsynchronousAudioSourceToAudioSourceAdapter (input_pipeline.audio_source);
        }
    }
}

