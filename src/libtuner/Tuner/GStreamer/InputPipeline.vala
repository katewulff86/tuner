/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.GStreamer {
    public class InputPipeline : Object {
        public AudioSource audio_source { get; private set; }

        public InputPipeline () {
            var src = Gst.ElementFactory.make ("autoaudiosrc", "src");
            var convert = Gst.ElementFactory.make ("audioconvert", "convert");
            audio_source = new AudioSource();
            var sink = Gst.ElementFactory.make ("fakesink", "sink");

            if (src == null | convert == null | sink == null)
                critical ("could not create all elements");

            var pipeline = new Gst.Pipeline("pipeline");
            pipeline.add_many(src, convert, audio_source, sink);

            if (!src.link (convert)) {
                critical ("could not link convert");
            }
            if (!convert.link (audio_source)) {
                critical ("could not link audio_source");
            }
            if (!audio_source.link (sink)) {
                critical ("could not link sink");
            }

            if (pipeline.set_state (Gst.State.PLAYING) == Gst.StateChangeReturn.FAILURE) {
                critical ("unable to play");
            }
        }
    }
}

