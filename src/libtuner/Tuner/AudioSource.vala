/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public interface AudioSource : Object {
        public abstract int sample_rate { get; protected set; }
        public abstract signal void sample_rate_changed (int sample_rate);
        public abstract void pull (double[] buffer);
        public abstract void set_maximum_length (int length);
    }
}

