/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp {
    public class FirstOrderFilter : Object, Filter {
        private double m1 = 0.0;
        private double a1;
        private double b0;
        private double b1;

        public FirstOrderFilter (double b0, double b1, double a1) {
            this.b0 = b0;
            this.b1 = b1;
            this.a1 = a1;
        }

        public double process_sample (double x) {
            var y = b0 * x + m1;
            m1 =    b1 * x - a1 * y;

            return y;
        }
    }
}

