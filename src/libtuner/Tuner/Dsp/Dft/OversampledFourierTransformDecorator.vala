/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp.Dft {
    public class OversampledFourierTransformDecorator : Object, FourierTransform {
        private FourierTransform fourier_transform;
        private double[] buffer;

        public int input_length { get; protected set; }
        public int result_length {
            get {
                return fourier_transform.result_length;
            }
            protected set {
            }
        }

        public OversampledFourierTransformDecorator (FourierTransform fourier_transform, int input_length) {
            this.fourier_transform = fourier_transform;
            this.input_length = input_length;
            this.buffer = new double[fourier_transform.input_length];
        }

        public unowned complex[] transform (double[] input)
                requires (input.length == input_length) {
            Posix.memcpy (buffer, input, sizeof (double) * input_length);

            return fourier_transform.transform (buffer);
        }
    }
}

