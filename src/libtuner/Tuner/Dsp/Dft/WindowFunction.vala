/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp.Dft {
    public delegate double[] WindowFunction (int length);

    public double[] rectangular_window (int length) {
        var window = new double[length];
        for (int n = 0; n < length; ++n)
            window[n] = 1.0;

        return window;
    }

    public double[] triangular_window (int length) {
        var window = new double[length];
        var N = length - 1;
        for (int n = 0; n < length; ++n)
            window[n] = 1.0 - Math.fabs (2 * (n - N/2) / N);

        return window;
    }

    public double[] welch_window (int length) {
        var window = new double[length];
        var N = length - 1;
        for (int n = 0; n < length; ++n)
            window[n] = 1.0 - Math.pow (2 * (n - N/2) / N, 2.0);

        return window;
    }

    public double[] sine_window (int length) {
        var window = new double[length];
        var N = length - 1;
        for (int n = 0; n < length; ++n)
            window[n] = Math.sin (Math.PI * n / N);

        return window;
    }

    public double[] hann_window (int length) {
        var window = new double[length];
        var N = length - 1;
        for (int n = 0; n < length; ++n)
            window[n] = 0.5 * (1 - Math.cos (2 * Math.PI * n / N));

        return window;
    }

    public void window_samples (double[] samples, double[] window)
            requires (samples.length == window.length) {
        for (int n = 0; n < samples.length; ++n)
            samples[n] *= window[n];
    }
}

