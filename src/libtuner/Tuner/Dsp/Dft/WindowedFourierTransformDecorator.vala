/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp.Dft {
    public class WindowedFourierTransformDecorator : Object, FourierTransform {
        private FourierTransform fourier_transform;
        private double[] buffer;
        private double[] window;

        public int input_length {
            get {
                return fourier_transform.input_length;
            }
            protected set {
            }
        }

        public int result_length {
            get {
                return fourier_transform.result_length;
            }
            protected set {
            }
        }

        public WindowedFourierTransformDecorator (FourierTransform fourier_transform, WindowFunction window_function) {
            this.fourier_transform = fourier_transform;
            this.buffer = new double[input_length];
            this.window = window_function (input_length);
        }

        public unowned complex[] transform (double[] input)
                requires (input.length == input_length) {
            for (int n = 0; n < input.length; ++n)
                buffer[n] = input[n] * window[n];

            return fourier_transform.transform (buffer);
        }
    }
}

