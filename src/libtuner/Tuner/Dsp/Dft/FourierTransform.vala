/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp.Dft {
    public interface FourierTransform : Object {
        public abstract int input_length { get; protected set; }
        public abstract int result_length { get; protected set; }
        public abstract unowned complex[] transform (double[] input);
    }
}

