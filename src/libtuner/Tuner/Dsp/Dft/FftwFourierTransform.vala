/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp.Dft {
    public class FftwFourierTransform : Object, FourierTransform {
        private double[] buffer;
        private complex[] spectrum;
        private Fftw.Plan fft_plan;

        public int input_length { get; protected set; }
        public int result_length { get; protected set; }

        public FftwFourierTransform (int length)
                requires (length >= 1) {
            input_length = length;
            buffer = new double[length];
            result_length = 1 + length / 2;
            spectrum = new complex[result_length];
            fft_plan = new Fftw.Plan.r2c_1d (buffer, spectrum, Fftw.Flags.ESTIMATE |  Fftw.Flags.DESTROY_INPUT);
        }

        public unowned complex[] transform (double[] samples)
                requires (samples.length == buffer.length) {
            copy_samples_to_buffer (samples);
            fft_plan.execute ();

            return spectrum;
        }

        private void copy_samples_to_buffer (double[] samples) {
            Posix.memcpy (buffer, samples, buffer.length * sizeof (double));
        }
    }
}

