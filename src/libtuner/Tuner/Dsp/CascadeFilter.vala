/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp {
    public class CascadeFilter : Object, Filter {
        public Filter[] filters;

        public CascadeFilter (Filter[] filters) {
            this.filters = filters;
        }

        public double process_sample (double x) {
            foreach (var filter in filters)
                x = filter.process_sample (x);

            return x;
        }

        public virtual void process_in_place (double[] input) {
            foreach (var filter in filters)
                filter.process_in_place (input);
        }

        public virtual void process (double[] input, double[] output)
                requires (input.length == output.length) {
            filters[0].process (input, output);
            for (int n = 1; n < filters.length; ++n)
                filters[n].process_in_place (output);
        }
    }
}

