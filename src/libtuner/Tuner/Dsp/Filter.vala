/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp {
    public interface Filter : Object {
        public abstract double process_sample (double input);
        public virtual void process_in_place (double[] input) {
            for (int n = 0; n < input.length; ++n)
                input[n] = process_sample (input[n]);
        }
        public virtual void process (double[] input, double[] output)
                requires (input.length == output.length) {
            for (int n = 0; n < input.length; ++n)
                output[n] = process_sample (input[n]);
        }
    }
}

