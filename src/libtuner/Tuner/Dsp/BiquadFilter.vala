/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp {
    public class BiquadFilter : Object, Filter {
        private double m1 = 0.0;
        private double m2 = 0.0;
        private double a1;
        private double a2;
        private double b0;
        private double b1;
        private double b2;

        public BiquadFilter (double b0, double b1, double b2, double a1, double a2) {
            this.b0 = b0;
            this.b1 = b1;
            this.b2 = b2;
            this.a1 = a1;
            this.a2 = a2;
        }

        public double process_sample (double x) {
            var y = b0 * x + m1;
            m1 =    b1 * x + m2 - a1 * y;
            m2 =    b2 * x      - a2 * y;

            return y;
        }
    }
}

