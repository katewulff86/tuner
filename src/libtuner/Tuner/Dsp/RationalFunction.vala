/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp {
    public struct RationalFunction {
        public Polynomial numerator;
        public Polynomial denominator;
    }

    public RationalFunction rational_function (double k, double[] real_zeros, complex[] complex_zeros, double[] real_poles, complex[] complex_poles) {
        var numerator = polynomial (k, real_zeros, complex_zeros);
        var denominator = polynomial (1.0, real_poles, complex_poles);

        return {numerator, denominator};
    }

    public complex rational_function_eval (RationalFunction rat, complex z) {
        var numerator = polynomial_eval (rat.numerator, z);
        var denominator = polynomial_eval (rat.denominator, z);

        return numerator / denominator;
    }

    public double rational_function_s_gain (RationalFunction rat, double omega) {
        var result = rational_function_eval (rat, complex.rect (0.0, omega));

        return result.square_abs ();
    }

    public double rational_function_z_gain (RationalFunction rat, double omega) {
        var result = rational_function_eval (rat, complex.polar (1.0, omega));

        return result.square_abs ();
    }

    public RationalFunction rational_function_empty () {
        return {polynomial_empty (), polynomial_empty ()};
    }

    public RationalFunction rational_function_clone (RationalFunction rat) {
        return {polynomial_clone (rat.numerator), polynomial_clone (rat.denominator)};
    }

    public int rational_function_degree (RationalFunction rat) {
        return polynomial_degree (rat.numerator) - polynomial_degree (rat.denominator);
    }

    public RationalFunction rational_function_mul (RationalFunction a, RationalFunction b) {
        var numerator = polynomial_mul (a.numerator, b.numerator);
        var denominator = polynomial_mul (a.denominator, b.denominator);

        return rational_function_cancel_roots ({numerator, denominator});
    }

    public RationalFunction rational_function_div (RationalFunction a, RationalFunction b) {
        var numerator = polynomial_mul (a.numerator, b.denominator);
        var denominator = polynomial_mul (a.denominator, b.numerator);

        return rational_function_cancel_roots ({numerator, denominator});
    }

    public RationalFunction rational_function_scale (RationalFunction rat, double scale) {
        var numerator = polynomial_scale (rat.numerator, scale);
        var denominator = polynomial_scale (rat.denominator, scale);

        return {numerator, denominator};
    }

    private void remove_common_elements<T> (Gee.MultiSet<T> a, Gee.MultiSet<T> b) {
        foreach (var element in a.to_array ()) {
            if (element in b) {
                a.remove (element);
                b.remove (element);
            }
        }
    }

    private RationalFunction rational_function_cancel_roots (RationalFunction rat) {
        var result = rational_function_clone (rat);
        remove_common_elements (result.numerator.real_roots, result.denominator.real_roots);
        remove_common_elements (result.numerator.complex_roots, result.denominator.complex_roots);

        return result;
    }

    public RationalFunction polynomial_invert (Polynomial poly) {
        var numerator = polynomial_empty ();
        numerator.k = poly.k;
        var denominator = polynomial_empty ();
        foreach (var root in poly.real_roots) {
            numerator.k *= root;
            numerator.real_roots.add (-1.0 / root);
            denominator.real_roots.add (0.0);
        }
        foreach (var root in poly.complex_roots) {
            numerator.k *= root.square_abs ();
            numerator.complex_roots.add (1.0 / root);
            denominator.real_roots.add (0.0);
            denominator.real_roots.add (0.0);
        }

        return {numerator, denominator};
    }

    public RationalFunction rational_function_invert (RationalFunction rat) {
        var numerator = polynomial_invert (rat.numerator);
        var denominator = polynomial_invert (rat.denominator);

        return rational_function_div (numerator, denominator);
    }

    public RationalFunction polynomial_bilinear_transform (Polynomial poly, double sample_rate = 1.0, double omega = 0.0) {
        var factor = 1.0;
        if (omega != 0.0)
            factor = omega / Math.tan (omega / (sample_rate * 2));
        var result = rational_function_empty ();
        var k = poly.k;

        foreach (var root in poly.real_roots) {
            k *= factor - root;
            result.numerator.real_roots.add ((factor + root) / (factor - root));
            result.denominator.real_roots.add (-1.0);
        }

        foreach (var root in poly.complex_roots) {
            k *= factor * factor - factor * 2 * root.real + root.square_abs ();
            result.numerator.complex_roots.add ((factor + root) / (factor - root));
            result.denominator.real_roots.add (-1.0);
            result.denominator.real_roots.add (-1.0);
        }

        result.numerator.k = k;

        return result;
    }

    public RationalFunction rational_function_bilinear_transform (RationalFunction rat, double sample_rate = 1.0, double omega = 0.0) {
        var numerator = polynomial_bilinear_transform (rat.numerator, sample_rate, omega);
        var denominator = polynomial_bilinear_transform (rat.denominator, sample_rate, omega);

        return rational_function_div (numerator, denominator);
    }
}

