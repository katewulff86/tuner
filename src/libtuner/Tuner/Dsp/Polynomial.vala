/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner.Dsp {
    public struct Polynomial {
        double k;
        Gee.MultiSet<double?> real_roots;
        Gee.MultiSet<complex?> complex_roots;
    }

    public Polynomial polynomial (double k, double[] real_roots, complex[] complex_roots) {
        var result = polynomial_empty ();
        result.k = k;
        foreach (var root in real_roots)
            result.real_roots.add (root);
        foreach (var root in complex_roots)
            result.complex_roots.add (root);

        return result;
    }

    public complex polynomial_eval (Polynomial poly, complex z) {
        var result = complex.rect (1, 0);
        foreach (var root in poly.real_roots) {
            var zero = z - root;
            result *= zero;
        }
        foreach (var root in poly.complex_roots) {
            var zero = z - root;
            result *= zero;
            zero = z - root.conj ();
            result *= zero;
        }

        return result * poly.k;
    }

    private int real_compare (double? a, double? b) {
        if (a < b)
            return -1;
        else if (a == b)
            return 0;
        else
            return 1;
    }

    private int complex_compare (complex? a, complex? b) {
        if (a.real < b.real)
            return -1;
        else if (a.real == b.real)
            return real_compare (Math.fabs (a.imag), Math.fabs (b.imag));
        else
            return 1;
    }

    public Polynomial polynomial_empty () {
        var real_roots = new Gee.TreeMultiSet<double?> (real_compare);
        var complex_roots = new Gee.TreeMultiSet<complex?> (complex_compare);

        return {1.0, real_roots, complex_roots};
    }

    public Polynomial polynomial_clone (Polynomial poly) {
        var clone = polynomial_empty ();
        clone.k = poly.k;
        clone.real_roots.add_all (poly.real_roots);
        clone.complex_roots.add_all (poly.complex_roots);

        return clone;
    }

    public int polynomial_degree (Polynomial poly) {
        return poly.real_roots.size + 2 * poly.complex_roots.size;
    }

    public Polynomial polynomial_mul (Polynomial a, Polynomial b) {
        var result = polynomial_clone (a);
        result.k *= b.k;
        result.real_roots.add_all (b.real_roots);
        result.complex_roots.add_all (b.complex_roots);

        return result;
    }

    public Polynomial polynomial_scale (Polynomial poly, double scale) {
        var result = polynomial_empty ();
        foreach (var root in poly.real_roots)
            result.real_roots.add (root * scale);
        foreach (var root in poly.complex_roots)
            result.complex_roots.add (root * scale);
        result.k = poly.k * Math.pow (scale, -polynomial_degree (result));

        return result;
    }
}

