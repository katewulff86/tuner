/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public class Analyser : Object {
        private Pda.PitchDetector pitch_detector;
        private AudioSource audio_source;
        private double[] buffer = {};

        public Analyser (Pda.PitchDetector pitch_detector, AudioSource audio_source) {
            this.pitch_detector = pitch_detector;
            this.audio_source = audio_source;
            this.audio_source.sample_rate_changed.connect (set_sample_rate);
            set_sample_rate (this.audio_source.sample_rate);
        }

        public double analyse () {
            audio_source.pull (buffer);

            return pitch_detector.detect (buffer);
        }

        private void set_sample_rate (int sample_rate) {
            pitch_detector.sample_rate = sample_rate;
            buffer = new double[pitch_detector.required_length];
            audio_source.set_maximum_length (pitch_detector.required_length);
        }
    }
}

