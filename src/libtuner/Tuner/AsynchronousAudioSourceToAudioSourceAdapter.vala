/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public class AsynchronousAudioSourceToAudioSourceAdapter : Object, AudioSource {
        private AsynchronousAudioSource audio_source;
        private Buffer buffer;

        public int sample_rate {
            get {
                return audio_source.sample_rate;
            }
            protected set {}
        }

        public AsynchronousAudioSourceToAudioSourceAdapter (AsynchronousAudioSource audio_source) {
            this.audio_source = audio_source;
            this.buffer = new Buffer (1024);
            this.audio_source.sample_rate_changed.connect ((sample_rate) => sample_rate_changed (sample_rate));
            this.audio_source.audio_available.connect ((samples) => this.buffer.push (samples));
        }

        public void pull (double[] samples) {
            buffer.pull (samples);
        }

        public void set_maximum_length (int length) {
            this.buffer = new Buffer (length);
        }
    }
}

