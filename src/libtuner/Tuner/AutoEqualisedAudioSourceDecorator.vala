/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public class AutoEqualisedAudioSourceDecorator : Object, AudioSource {
        private AudioSource audio_source;
        public int sample_rate {
            get {
                return audio_source.sample_rate;
            }
            protected set {
            }
        }

        public AutoEqualisedAudioSourceDecorator (AudioSource audio_source) {
            this.audio_source = audio_source;
            audio_source.sample_rate_changed.connect ((sample_rate) => {
                sample_rate_changed (sample_rate);
            });
        }

        public void pull (double[] buffer) {
            audio_source.pull (buffer);
        }

        public void set_maximum_length (int length) {
            audio_source.set_maximum_length (length);
        }
    }
}

