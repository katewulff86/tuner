/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public struct Complex {
        double r;
        double i;
    }

    public double square_magnitude (Complex z) {
        return z.r*z.r + z.i*z.i;
    }

    public double magnitude (Complex z) {
        return Math.sqrt (square_magnitude (z));
    }

    public double argument (Complex z) {
        return Math.atan2 (z.i, z.r);
    }

    public double log_magnitude (Complex z) {
        return 0.5 * square_magnitude (z);
    }

    public Complex polar (double r, double theta) {
        return {r * Math.cos (theta), r * Math.sin (theta)};
    }

    public Complex complex_conj (Complex z) {
        return {z.r, -z.i};
    }

    public Complex complex_neg (Complex z) {
        return {-z.r, -z.i};
    }

    public Complex complex_add (Complex a, Complex b) {
        return {a.r + b.r, a.i + b.i};
    }

    public Complex complex_add_real (Complex a, double b) {
        return {a.r + b, a.i};
    }

    public Complex complex_sub (Complex a, Complex b) {
        return {a.r - b.r, a.i - b.i};
    }

    public Complex complex_sub_real (Complex a, double b) {
        return {a.r - b, a.i};
    }

    public Complex complex_real_sub (double a, Complex b) {
        return {a - b.r, -b.i};
    }

    public Complex complex_mul (Complex a, Complex b) {
        return {a.r * b.r - a.i * b.i, a.r * b.i + a.i * b.r};
    }

    public Complex complex_mul_real (Complex a, double b) {
        return {a.r * b, a.i * b};
    }

    public Complex complex_div (Complex a, Complex b) {
        var scale = 1.0 / square_magnitude (b);

        return {scale * (a.r * b.r + a.i * b.i), scale * (a.i * b.r - a.r * b.i)};
    }

    public Complex complex_div_real (Complex a, double b) {
        var inv = 1.0 / b;

        return {inv * a.r, inv * a.i};
    }

    public Complex complex_real_div (double a, Complex b) {
        var scale = a / square_magnitude (b);

        return {scale * b.r, -scale * b.i};
    }

    public Complex complex_inv (Complex z) {
        return complex_real_div (1.0, z);
    }
}

