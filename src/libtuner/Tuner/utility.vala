/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public void spectrum_square_magnitude (complex[] spectrum, double[] result)
            requires (spectrum.length == result.length) {
        for (int n = 0; n < spectrum.length; ++n)
            result[n] = spectrum[n].square_abs ();
    }

    public void spectrum_log_magnitude (complex[] spectrum, double[] result)
            requires (spectrum.length == result.length) {
        for (int n = 0; n < spectrum.length; ++n)
            result[n] = 0.5 * Math.log (spectrum[n].square_abs ());
    }

    public void log_transform (double[] samples) {
        for (int n = 0; n < samples.length; ++n)
            samples[n] = Math.log (samples[n]);
    }

    public int harmonic_number (double fundamental, double harmonic) {
        double lower = fundamental;
        double upper = harmonic;
        if (fundamental > harmonic) {
            lower = harmonic;
            upper = fundamental;
        }
        var multiple = upper / lower;
        var number = (int) Math.round (multiple);
        if (Math.fabs (multiple - number) > 0.05)
            return 0;
        if (fundamental < harmonic)
            return number;
        else if (number == 1)
            return 1;
        else
            return -number;
    }

    public void normalise_stats (double[] input)
            requires (input.length > 0) {
        var avg = average (input);
        var std = std (input);
        var factor = std == 0.0 ? 1.0 : 1.0 / std;

        for (int n = 0; n < input.length; ++n)
            input[n] = (input[n] - avg) * factor;
    }

    public void normalise_average (double[] input)
            requires (input.length > 0) {
        var avg = average (input);

        for (int n = 0; n < input.length; ++n)
            input[n] -= avg;
    }

    public double average (double[] input)
            requires (input.length > 0) {
        var avg = 0.0;
        foreach (var x in input)
            avg += x;

        return avg / input.length;
    }

    public double std (double[] input)
            requires (input.length > 0) {
        var avg = average (input);
        var std = 0.0;

        foreach (var x in input)
            std += (x - avg) * (x - avg);

        return Math.sqrt (std / input.length);
    }

    public double cents (double ratio) {
        return 1200.0 * Math.log2 (ratio);
    }
}

