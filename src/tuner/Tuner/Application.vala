/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public class Application : Adw.Application {
        private AudioPlumber audio_plumber;
        private AnalyserTimeout analyser_timeout;

        public Application () {
            Object (application_id: "me.katewulff.Tuner", flags: ApplicationFlags.FLAGS_NONE);
        }

        construct {
            ActionEntry[] action_entries = {
                { "about", this.on_about_action },
                { "preferences", this.on_preferences_action },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", {"<primary>q"});
        }

        public override void activate () {
            base.activate ();
            var win = this.active_window;
            if (win == null) {
                win = new Tuner.Window (this);
                make_timeout_and_plumber (out audio_plumber, out analyser_timeout);
                analyser_timeout.pitch_detected.connect ((pitch) => {
                    stderr.printf ("%f\n", pitch);
                });
            }
            win.present ();
        }

        private void on_about_action () {
            string[] developers = { "Kate Wulff" };
            var about = new Adw.AboutWindow () {
                transient_for = this.active_window,
                application_name = "Tuner",
                application_icon = "me.katewulff.Tuner",
                developer_name = "Kate Wulff",
                version = "0.1.0",
                developers = developers,
                copyright = "© 2022 Kate Wulff",
            };

            about.present ();
        }

        private void on_preferences_action () {
            message ("app.preferences action activated");
        }
    }
}

