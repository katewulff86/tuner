/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    [GtkTemplate (ui = "/me/katewulff/Tuner/ui/window.ui")]
    public class Window : Adw.ApplicationWindow {
        // [GtkChild]
        // private unowned Gtk.Label label;

        public Window (Gtk.Application app) {
            Object (application: app);
        }
    }
}

