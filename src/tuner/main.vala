/* Copyright 2022 Kate Wulff
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Tuner {
    public static int main (string[] args) {
        var app = new Application ();
        Gst.init (ref args);

        return app.run (args);
    }
}

